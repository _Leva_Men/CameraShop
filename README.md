# CameraShop
Python final project

Даный проект написан для того чтобы зашитить финальный проект на курсах
и сдать курсовой проект в университете.

Используемые технологии:
1. Django
2. Bootstrap 4
3. PostgresSQL

UML диаграма:
![Database diagram](database_camera_shop.png)
